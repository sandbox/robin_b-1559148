﻿Metriweb is an analytic tool maintained by CIM.be for official visitor statistics.

This module adds the tracker code of Metriweb to the content pages of Drupal. 

The module takes the arguments (supplied by arg()) as keyword identifier. 
Because the keyword can only be 24 characters long (living in the 90's), the argument value can be truncated.

Note: this module is not developed by CIM and does include the necessary javascript-file provided by CIM. 
You should get the js-file from them when you signup for the service.

The admin pages will not be tracked.


Install:

1) Go to admin/modules/list
2) Activate this module
3) Fill in the CONSTANTS in metriweb.module (big boys like you don't need an adminpage)
4) Copy the metriWeb access file you received from metriweb (mw-complexhash.txt) into this folder 
5) Copy the mwTag.js file you received from metriweb into this folder.


Module developed by Robin Brackez
Sponsor: DeWereldMorgen.be


Javascript code (mwTag.js) is developed by DouWère/MetriWare (c) 2000-2010 (not included)